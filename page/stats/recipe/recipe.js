const {ipcRenderer} = require('electron');

let back = document.getElementById('back');

back.addEventListener('click', () => {
      ipcRenderer.send('backDashboard')
})

let btnVisit = document.getElementById('#visitStats')
let yearInit = new Date().getFullYear();
let year;
let btnList = document.getElementById('buttonList');

for (let i = 0; i < 5; i++) {
      btnList.insertAdjacentHTML('beforeend', '<button data-select-year class="btn btn-dark m-2">' + ( yearInit - i ) + '</button>')
}
let btnYears = document.querySelectorAll('[data-select-year]');

function checkAvg (value, moyenne) {
      value.forEach( e => {
            if (e.innerHTML > moyenne) {
                  e.classList.add('vert')
            } else {
                  e.classList.add('rouge')
            }
      })
}

function insertDataTable (data, year ) {
      let dashboard = document.getElementById('dashboard');
      let table = dashboard.querySelector('table');
      table.innerHTML = '';
      let total = 0;

      console.log(data)

      if (data.values.length === 0) {
            table.insertAdjacentHTML("beforeend", '<tr><td>Aucune données pour cette année</tr></td>');
      }

      data.values.forEach( e => {
            let string = '';
            let customDate = new Date(year + '-' + e.month + '-' + '01');

            total += e.nbCreated;

            string += '<tr>';
            string += '<td>En<strong> ' + customDate.toLocaleString('default', {month: 'long'}) + ' ' + '</strong><span id="span">' + e.nbCreated + '</span>' + ' recettes on été postées </td>';
            string += '</tr>';

            table.insertAdjacentHTML("beforeend", string);
      })

      let moyenne = total / data.values.length;
      let span = document.querySelectorAll('#span')

      checkAvg(span, moyenne);
}

fetch('http://127.0.0.1:8000/api/recipe_stats?year=' + yearInit, {
      method: 'GET',
      headers: {
            'Accept': 'application/json'
      },
}).then((response) => {
      return response.json();
}).then((data) => {
      insertDataTable(data, yearInit)
})

btnYears.forEach(e => {
      e.addEventListener('click', () => {
            year = e.innerHTML;
            fetch('http://127.0.0.1:8000/api/recipe_stats?year=' + year, {
                  method: 'GET',
                  headers: {
                        'Accept': 'application/json'
                  },
            }).then((response) => {
                  return response.json();
            }).then((data) => {
                  insertDataTable(data, year)
            })
      })
})
