const {ipcRenderer} = require('electron');

let back = document.getElementById('back');

back.addEventListener('click', () => {
      ipcRenderer.send('backDashboard')
})

function checkAvg (value, moyenne) {
      value.forEach( e => {
            if (e.innerHTML > moyenne) {
                  e.classList.add('vert')
            } else {
                  e.classList.add('rouge')
            }
      })
}

fetch('http://127.0.0.1:8000/api/ingredient_stats',{
      method: 'GET',
      headers: {
            'Accept': 'application/json'
      },
}).then((response) => {
      return response.json();
}).then((data) => {
      let div = document.getElementById('dashboard')
      let total = 0;

      data.values.forEach( e => {
            let string = '';
            total += e.nb;

            string += '<p>' + "L'ingredient : <strong>" + e.name + '</strong> est utilisé dans <strong><span id="span">' + e.nb + '</span></strong> recettes !' + '</p>';
             div.insertAdjacentHTML('beforeend', string);

            let moyenne = total / data.values.length;
            let span = document.querySelectorAll('#span');

            checkAvg(span, moyenne);
      })
})