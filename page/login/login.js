let name = document.getElementById('name');
let psw = document.getElementById('psw');
let submit = document.getElementById('submit');
let h2 = document.getElementById('h2');

const jwt_decode = require('jwt-decode');
const {ipcRenderer} = require("electron");

submit.addEventListener('submit', (e) => {
      ipcRenderer.send('openLoad',)

      e.preventDefault();

      fetch('http://127.0.0.1:8000/api/login_check', {
            method: 'POST',
            headers: {
                  'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                  email: name.value,
                  password: psw.value,
            })
      }).then((response) => {
            return response.json();
      }).then((data) => {
            ipcRenderer.send('closeLoad',)

            if (data.code === 401) {
                  h2.innerHTML = data.message;
            } else {
                  let decoded = jwt_decode(data.token);

                  if(!decoded.roles.includes('ROLE_ADMIN') ) {
                        h2.innerHTML = "Vous n'avez pas les droits"
                  } else {
                        ipcRenderer.send('openDashboard')
                        ipcRenderer.send('sendToken', data.token)
                  }
            }
      })
})
