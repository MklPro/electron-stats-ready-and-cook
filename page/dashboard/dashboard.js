const {ipcRenderer} = require('electron');

let btnVisit = document.getElementById('visit')
let btnRecipe = document.getElementById('recipe')
let btnIngredient = document.getElementById('ingredient')

btnVisit.addEventListener('click', () => {
      ipcRenderer.send('openVisitWindow')
})

btnRecipe.addEventListener('click', () => {
      ipcRenderer.send('openRecipeWindow')
})

btnIngredient.addEventListener('click', () => {
      ipcRenderer.send('openIngredientWindow')
})