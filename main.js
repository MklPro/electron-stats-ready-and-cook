// Modules to control application life and create native browser window
const {app, BrowserWindow, ipcMain, Notification, ipcRenderer} = require('electron')
const path = require('path')
let testWindow = null;
let loginWindow = null;
let loadWindow = null;
let token = null;
let visitStatWindow = null;
let dashBoardWindow = null;
let recipeStatWindow = null;
let ingredientStatWindow = null;
let cron = require('node-cron');
const jwt_decode = require("jwt-decode");
const fetch = require('electron-fetch').default;

function createWindow() {
      // Create the browser window.
      loginWindow = new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
                  nodeIntegration: true,
                  contextIsolation: false,
                  // preload: path.join(__dirname, 'preload.js')
            }
      })

      // and load the index.html of the app.
      // testWindow.loadFile('page/test/test.html')
      loginWindow.loadFile('page/login/login.html');
      loginWindow.maximize();

      // Open the DevTools.
      // mainWindow.webContents.openDevTools()
}
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.

app.whenReady().then(() => {
      createWindow()

      cron.schedule('* * * * *', () => {

            let dateString = getDateMinus5MinutesString();

            function getDateMinus5MinutesString() {
                  let date = new Date();
                  let newDate = new Date(date.getTime() - 5 * 60000);
                  return newDate.toLocaleDateString('fr-CA') + "%20" + newDate.getHours() + ":" + newDate.getMinutes() + ":" + newDate.getSeconds();
            }

            fetch('http://127.0.0.1:8000/api/recipes?publishedAt[after]=' + dateString, {
                  method: 'GET',
                  headers: {
                        'Accept': 'application/json'
                  },
            }).then((response) => {
                  return response.json();
            }).then((data) => {
                  if(data.length > 0 ) {
                        const title = 'Nouvelle recette !!';
                        for ( let i= 0; i < data.length; i++ ) {
                              new Notification({ title: title, body: 'La recette ' + data[i].name + ' a été ajouté le ' + data[i].publishedAt }).show();
                        }
                  }
            })
      });
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
      if (process.platform !== 'darwin') app.quit()
})

ipcMain.on('closeTestWindow', (event, arg) => {
      testWindow.close();
})

ipcMain.on('closeLoad', (event, arg) => {
      loadWindow.close();
})

ipcMain.on('openLoad', (event, arg) => {
      loadWindow = new BrowserWindow({
            width: 300,
            height: 300,
            center: true,
            resizable: false,
            movable: false,
            frame: false,
            alwaysOnTop: true,
            webPreferences: {
                  nodeIntegration: true,
                  contextIsolation: false,
            },
      })
      loadWindow.loadFile('page/load/load.html');
})

ipcMain.on('openDashboard', (event, arg) => {
      dashBoardWindow = new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
                  nodeIntegration: true,
                  contextIsolation: false,
            },
      })
      dashBoardWindow.loadFile('page/dashboard/dashboard.html');
      dashBoardWindow.maximize();
})

ipcMain.on('sendToken', (event, arg) => {
      token = arg;
      loginWindow.close();
})

ipcMain.on('value', (event, arg) => {
      event.reply('valueReply', 'test')
})

ipcMain.on('openVisitWindow', () => {
      visitStatWindow = new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
                  nodeIntegration: true,
                  contextIsolation: false,
            },
      })
      visitStatWindow.loadFile('page/stats/visit/visit.html');
      visitStatWindow.maximize();
      dashBoardWindow.close();
})

ipcMain.on('openRecipeWindow', () => {
      recipeStatWindow = new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
                  nodeIntegration: true,
                  contextIsolation: false,
            },
      })
      recipeStatWindow.loadFile('page/stats/recipe/recipe.html');
      recipeStatWindow.maximize();
      dashBoardWindow.close();
})

ipcMain.on('openIngredientWindow', () => {
      ingredientStatWindow = new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
                  nodeIntegration: true,
                  contextIsolation: false,
            },
      })
      ingredientStatWindow.loadFile('page/stats/ingredient/ingredient.html');
      ingredientStatWindow.maximize();
      dashBoardWindow.close();
})
ipcMain.on('backDashboard', () => {
      dashBoardWindow = new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
                  nodeIntegration: true,
                  contextIsolation: false,
            },
      })
      dashBoardWindow.loadFile('page/dashboard/dashboard.html');
      dashBoardWindow.maximize();

      if (visitStatWindow != null) {
            visitStatWindow.close();
            visitStatWindow = null;
      }

      if (recipeStatWindow != null) {
            recipeStatWindow.close();
            recipeStatWindow = null;
      }

      if (ingredientStatWindow != null) {
            ingredientStatWindow.close();
            ingredientStatWindow = null;
      }
})
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.